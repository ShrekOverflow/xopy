import React from 'react';

export default function Image({ url }) {
    return (
        <div className="ui-frame__image">
            <img src={url} />
        </div>
    );
}
