import React from 'react';
import UIFrame from './UIFrame';
import autobind from 'core-decorators/lib/autobind';

@autobind class FramesView extends React.Component{
    render(){
        const {frames, active} = this.props;
        const params = {
            onInit: this.onInit,
            loop: true,
        };
        return (
            <div className="ui-pasteboard">
                {frames.map((frame, index) => (
                    <div key={frame.id}>
                        <UIFrame
                            isActive={active === index}
                            frame={frame}
                        />
                    </div>
                ))}
            </div>
        );
    }
}

export default FramesView;
