import { clipboard } from 'electron';
import { EventEmitter } from 'events';
import autobind from 'core-decorators/lib/autobind';
import debug from 'debug';
import ClipboardFrame from './Frame';

const log = debug('clipboard:watcher');

@autobind class ClipboardWatcher extends EventEmitter {
    constructor(freq) {
        super();
        this._delay = 1000 / freq;
        this._currentFrame = this._getFrameFromBuffer();
        this.on('newListener', this._handleAddListener);
        this.on('removeListener', this._handleRemoveListener);
    }

    getCurrentFrame() {
        if (this._isWatching) {
            return this._currentFrame;
        }
        return this._getFrameFromBuffer();
    }

    _checkForClipboardChanges() {
        log("Check clipboard for changes");
        const newFrame = this._getFrameFromBuffer();
        const currentFrame = this._currentFrame;

        if (currentFrame.isEqual(newFrame)) {
            return;
        }
        
        log("Initializing new frame");
        newFrame.initializeMeta();
        log("Emit new frame");
        this._currentFrame = newFrame;
        this.emit('frame', newFrame);
    }

    _startWatcher() {
        log("Starting watcher");
        this._isWatching = true;
        this._currentFrame = this._getFrameFromBuffer();
        this._watcher = setInterval(this._checkForClipboardChanges, this._delay);
    }

    _stopWatcher() {
        log("Stopping watcher");
        this._isWatching = false;
        clearInterval(this._watcher);
    }

    _getFrameFromBuffer() {
        /* Just writing the entire frame is better */
        log("Polling pasteboard");
        
        const bookmark = clipboard.readBookmark(),
            image = clipboard.readImage(),
            html = clipboard.readHTML(),
            text = clipboard.readText(),
            rtf = clipboard.readRTF();

        return new ClipboardFrame(image, text, rtf, bookmark, html);
    }

    _handleAddListener(eventName) {
        if (!this._isWatching && eventName === 'frame') {
            this._startWatcher();
        }
    }

    _handleRemoveListener(evName) {
        if (this._isWatching && evName === 'frame' && this.listenerCount('frame') <= 1) {
            this._stopWatcher();
        }
    }
}

export default ClipboardWatcher; 