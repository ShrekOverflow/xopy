import autobind from 'core-decorators/lib/autobind';
import { clipboard } from 'electron';
import XXHash from 'xxhash';

@autobind class ClipboardFrame {
    static _hashImage(frame) {
        return XXHash.hash64(frame.image.toBitmap(), 0xCAFEBABE).toString('hex');
    }
    

    static getFrameType(clipboardFrame) {
        
        if (clipboardFrame.type) {
            return clipboardFrame.type;
        }

        if (clipboardFrame.image.isEmpty() === false) {
            return 'image';
        }

        if (clipboardFrame.bookmark.url && clipboardFrame.bookmark.title) {
            return 'bookmark';
        }

        if (clipboardFrame.html) {
            return 'html';
        }

        if (clipboardFrame.rtf) {
            return 'rtf';
        }

        if (clipboardFrame.text) {
            return 'text';
        }

        return 'empty';
    }

    static areEqual(clipboardFrameA, clipboardFrameB) {

        if (clipboardFrameA.id !== undefined && (clipboardFrameA.id === clipboardFrameB.id)) {
            return true;
        }

        if (clipboardFrameA.type === clipboardFrameB.type) {
            if (clipboardFrameA.type === 'image') {
                const imageSizeA = clipboardFrameA.image.getSize();
                const imageSizeB = clipboardFrameB.image.getSize();
                if (imageSizeA.height === imageSizeB.height
                    && imageSizeA.width === imageSizeB.width) {
                    clipboardFrameA._hash = clipboardFrameA.hash || ClipboardFrame._hashImage(clipboardFrameA);
                    clipboardFrameB._hash = clipboardFrameB.hash || ClipboardFrame._hashImage(clipboardFrameB);
                    if (clipboardFrameA._hash === clipboardFrameB._hash) {
                        return true;
                    }
                }
            } else if (clipboardFrameA.type === 'bookmark') {
                const bookmarkA = clipboardFrameA.bookmark;
                const bookmarkB = clipboardFrameB.bookmark;

                if (bookmarkA.url === bookmarkB.url && bookmarkA.title === bookmarkB.title) {
                    return true;
                }
            } else {
                /* Text types here we can simply compare */
                const textA = clipboardFrameA[clipboardFrameA.type];
                const textB = clipboardFrameB[clipboardFrameB.type];
                if (textA === textB) {
                    return true;
                }
            }
        }

        return false;

    }

    constructor(image, text, rtf, bookmark, html) {
        this.image = image;
        // Maybe improve this in future or tap into windows native events and use a watcher
        // only on mac.
        this.text = text;
        this.rtf = rtf;
        this.bookmark = bookmark;
        this.html = html;
        this.type = ClipboardFrame.getFrameType(this);
        // @TODO: Object.freeze(this);
    }


    initializeMeta(){
        this.id = 'frame_' + Date.now();
        if(this.type === 'image'){
            this.thumbnail = this.image.resize({
                width: 480
            }).toDataURL();
        }
    }

    isEqual(otherFrame) {
        return ClipboardFrame.areEqual(this, otherFrame);
    }

    copyToClipboard(){
        clipboard.write(this);
    }
}

export default ClipboardFrame;

