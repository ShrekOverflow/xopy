import React from 'react';

export default function Text({data}) {
    return (
        <div className="ui-frame__text">
            <code>
                {data}
            </code>
        </div>
    );
}
