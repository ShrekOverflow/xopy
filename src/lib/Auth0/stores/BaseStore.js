import autobind from 'core-decorators/lib/autobind';

@autobind
class BaseStore {
    constructor(service) {
        this.service = service;
    }

    get() {
        throw new Error('Each adapter must implement <string> get(accountName)');
    }

    set() {
        throw new Error('Each adapter must implement <string> set(accountName, password)')
    }

    delete() {
        throw new Error('Each adapter must implement <string> delete(accountName)')
    }
}

export default BaseStore;