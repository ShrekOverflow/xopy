import autobind from 'core-decorators/lib/autobind';
import Session from '../Session';

@autobind
class BaseAdapter{    
    constructor(domain, packageIdentifier){
        this.domain = domain;
        this.packageIdentifier = packageIdentifier;
    }

    attachCallbackHandler(fn){
        throw new Error('Each adapter must implement <void> attachCallbackHandler(fn)')
    }

    getPlatformName(){
        throw new Error('Each adapter must implement <string> getPlatformName()');
    }

    awaitCallback(){
        return new Promise(
            (resolve, reject) => Session.queueForCallback(resolve, reject, this)
        );
    }

    canHandle(url) {
        console.log(url);
        const cbUrl = this.getRedirectURL();
        return url.indexOf(cbUrl) !== -1;
    }

    getRedirectURL(){
        return `${this.packageIdentifier}://${this.domain}/${this.getPlatformName()}/${this.packageIdentifier}/callback`;
    }
}

export default BaseAdapter;