import keytar from 'keytar';
import BaseStore from './BaseStore';
import autobind from 'core-decorators/lib/autobind';

class ElectronKeyTarStore extends BaseStore {
    get(accountName) {
        return keytar.getPassword(this.service, accountName);
    }

    set(accountName, password) {
        if (keytar.getPassword(accountName)) {
            keytar.deletePassword(this.service, accountName);
        }
        return keytar.addPassword(this.service, accountName, password);
    }

    delete(accountName) {
        if (keytar.deletePassword(this.service, accountName) === null) {
            throw new Error(`Unable to delete password for ${this.service}->${accountName}`);
        }
    }
}