/* As of now this does nothing, it should however handle connections etc */
import React from 'react';
import ReactDOM from 'react-dom';
import App from '../components/App';
import Clipboard from '../lib/Clipboard';
import debug from 'debug';
import autobind from 'core-decorators/lib/autobind';
import {EventEmitter} from 'events';

const log = debug('runtime:core');

@autobind class CoreRuntime extends EventEmitter{
    constructor(config = {
        maxFramesToStore: 30,
        checkFrequency: 2,
        appContainer: '#app',
    }) {
        super();
        try{
            log('Initializing');
            this.config = config;
            this.user = null;
            this.frames = [];
            this.currentActiveIndex = 0;
            log('Attaching keyboard listener');
            this.clipboard = new Clipboard(config.checkFrequency);
            this.clipboard.on('frame', this._handleNewFrame);
            log('Rendering app');
            ReactDOM.render(<App runtime={this}/>, document.querySelector(config.appContainer));
        }catch(e){
            console.log(e);
        }
    }

    /* yep */
    _emitFrames(){
        this.emit('frames-updated', this.frames.slice(0));
    }

    _handleNewFrame(frame) {
        log('New frame', frame);
        this.frames.unshift(frame);
        if (this.frames.length >= this.config.maxFramesToStore) {
            log('Overflow releaseing frame that was least used recently');
            this.frames.pop();
        }
        this._emitFrames();
    }

    release(index) {
        const [frame] = this.frames.splice(index, 1);
        if (frame) {
            console.log(frame);
            frame.copyToClipboard();
        }
        this.emit('release');
        this._emitFrames();
    }

    handleOpenURL(url) {
        log('Open URL called with', url);
        this.user = null;
    }
}

export default CoreRuntime;
