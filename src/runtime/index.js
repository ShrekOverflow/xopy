import debug from 'debug';
const log = debug('runtime');
log('Intiated application');
log('Detecting Runtime');
const runtimeName = 'electron';

let runtime = null;
log(`${runtimeName} detected, loading ${runtimeName} runtime`);

if (runtimeName === 'electron') {
    const Runtime = require('./electron').default;
    log('Runtime loaded');
    try {
        runtime = new Runtime();
    } catch (e) {
        log('Failed to properly launch the application', e);
    }
    log('App is now running');
}

export default runtime;
