import $ from 'nodobjc';
import debug from 'debug';


const log = debug('utils:native:osx');
class DarwinUtils {
    // @TODO: Is this needed? Can't we just use the electron default? Or is this even needed at all?

    constructor() {
        log('Loading AppKit');
        $.import('AppKit');
        log('Loading Carbon');
        $.import('Carbon');
        log('Creating Pool');
        this.pool = $.NSAutoreleasePool('alloc')('init');
        log('Loaded darwin utils', this);
        this.oldProcessIdentifier = null;
    }

    recordCurrentFocus() {
        log('Attempting to sniff frontmost process identifier');
        const frontMostApp = $.NSWorkspace('sharedWorkspace')('frontmostApplication');
        this.oldProcessIdentifier = frontMostApp('processIdentifier');
        log('Sniffed ', this.oldProcessIdentifier);
    }

    refocusLast() {
        log('Attempting to fetch process by last known frontmost process identifier', this.oldProcessIdentifier);
        const appToResume = $.NSRunningApplication('runningApplicationWithProcessIdentifier', this.oldProcessIdentifier);
        log('Attemptint to focus & active the application');
        appToResume('activateWithOptions', $.NSApplicationActivateIgnoringOtherApps);
        this.oldProcessIdentifier = null;
        log('Releasing');
    }

    runPasteCommand() {
        log('Executing paste command via', this);
        log('Creating Event Source and keys');
        const source = $.CGEventSourceCreate($.kCGEventSourceStateHIDSystemState);
        const pasteCommandDown = $.CGEventCreateKeyboardEvent(source, $.kVK_ANSI_V, true);
        const pasteCommandUp = $.CGEventCreateKeyboardEvent(source, $.kVK_ANSI_V, false);

        log('Simulating Cmd+V via System Events');
        $.CGEventSetFlags(pasteCommandDown, $.kCGEventFlagMaskCommand);

        $.CGEventPost($.kCGAnnotatedSessionEventTap, pasteCommandDown);
        $.CGEventPost($.kCGAnnotatedSessionEventTap, pasteCommandUp);

        log('Releasing allocated items');
        $.CFRelease(pasteCommandUp);
        $.CFRelease(pasteCommandDown);
        $.CFRelease(source);
        log('Paste complete');        
    }
}

export default DarwinUtils;
