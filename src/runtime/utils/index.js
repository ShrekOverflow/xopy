import os from 'os';

export default new (require(`./native.${os.platform()}`).default)();

