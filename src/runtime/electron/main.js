// Electron is a little bit as the main process and the renderer are completely different
import { app, globalShortcut, BrowserWindow, ipcMain, dialog, screen } from 'electron';
import autobind from 'core-decorators/lib/autobind';
import debug from 'debug';
import path from 'path';
import utils from '../utils';
import Positioner from 'electron-positioner';

const log = debug('runtime:electron');
const OVERLAY_HTML_PATH = `file://${path.join(__dirname, '../../ui/app.html')}`;

// The point of writing this was to allow electron to bootup, and handle context switching
// via a shortcut 

@autobind class ElectronRuntime {
    constructor(config = { debug: false }) {
        if (app.makeSingleInstance(() => {})) {
            log('Application is already running, quitting');
            dialog.showErrorBox('Xopy is already running', 'Another instance of Xopy is already running. This instance will now quit.');
            app.quit();
        } else {
            log('Starting application now');
            this.config = config;

            app.on('ready', this.bootstrap);
        }
    }

    handleTrigger() {
        if (this.overlay.isVisible() === false){
            utils.recordCurrentFocus();
            this.overlay.webContents.send('open');
            this.overlay.show();
            return;
        }
        this.overlay.webContents.send('next');
    }

    handleRelease() {
        this.overlay.hide();
        utils.refocusLast();
        setTimeout(function (){
            utils.runPasteCommand();
        }, 200);
    }

    handleOverlayBlurred() {
        if (this.overlay.isVisible()) {
            this.overlay.focus();
        }
    }

    bootstrap() {
        app.dock.hide();
        app.setAsDefaultProtocolClient('com.hingnikar.xopy');
        
        this.overlay = new BrowserWindow({
            width: 360,
            height: 240,
            borderless: true,
            frame: false,
            show: false,
            focusable: true,
            resizeable: false,
            movable: false,
            vibrancy: 'light',
            hasShadow: false,
            alwaysOnTop: true,
            transparent: true,
        });

        const positioner = new Positioner(this.overlay);
        const optimumPosition = positioner.calculate('bottomCenter');
        
        this.overlay.loadURL(OVERLAY_HTML_PATH);
        
        optimumPosition.y *= 0.9;
        this.overlay.setPosition(optimumPosition.x, optimumPosition.y);

        if (this.config.debug) {
            this.overlay.webContents.openDevTools();
        }

        this.overlay.on('blur', this.handleOverlayBlurred);
        globalShortcut.register('CommandOrControl+Shift+V', this.handleTrigger);
        ipcMain.on('release', this.handleRelease);
    }
}

export default ElectronRuntime;
