import autobind from 'core-decorators/lib/autobind';
import os from 'os';
import React from 'react';
import FramesView from './FramesView';

@autobind class App extends React.Component {
    constructor() {
        super();
        this.state = {
            active: 0,
            frames: []
        };
    }

    gotoStart(){
        this.setState({
            active: 0
        });
    }

    nextFrame(){
        let {active, frames} = this.state;
        active += 1;
        active %= frames.length;
        this.setState({ active });
    }

    componentDidMount() {

        const { props, state } = this;
        const { runtime } = props;

        runtime.on('frames-updated', (frames) => this.setState({ frames }));
        runtime.on('open', this.gotoStart);
        runtime.on('next', this.nextFrame);
        document.addEventListener('keyup', (event) => {
            if (event.which === 91 || (os.platform() !== 'darwin' && event.which === 17)) {
                runtime.release(this.state.active);
            }
        });
    }

    render() {
        const { frames, active } = this.state;
        return (
            <div className="clipboard">
                <FramesView
                    ref='frameview'
                    frames={frames}
                    active={active}
                />
            </div>
        );
    }
}

export default App;
