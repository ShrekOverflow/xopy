import PKCEAuth from './PKCEAuth';
import ElectronBrowserAdapter from './adapters/Electron';
import ElectronKeytarStorage from './stores/Electron';
import autobind from 'core-decorators/lib/autobind';
import decodeJwt from 'jwt-decode';
import request from 'request-promise';

function getBrowserAdapterCtr() {
    return ElectronBrowserAdapter;
}

function getStoreCtr() {
    return ElectronKeytarStorage;
}

class Auth0 {
    constructor(config, params) {
        const { packageIdentifier, domain } = config;
        this.browser = new (getBrowserAdapterCtr())(domain, packageIdentifier);
        this.store = new (getStoreCtr())(packageIdentifier);
        this.config = config;
        this.params = params;
        this.token = null;
    }

    isTokenExpired() {
        if (!this.token) {
            return true;
        }

        return decodeJwt(this.token).exp < Date.now() / 1000;
    }

    async isLoggedIn() {
        if (!this.isTokenExpired()) {
            return true;
        }
    }

    async executeRefreshTokenFlow() {
        const { user, config } = this;
        const { domain, clientId } = config;
        const refreshToken = this.store.get(user.user_id);
        if (!refreshToken) {
            throw new Error('No refresh token stored.');
        }
        const authResult = await request.post(`https://${domain}/oauth/token`, {
            json: true,
            data: {
                refresh_token: refreshToken,
                grant_type: 'refresh_token',
                client_id: clientId,
                scope: 'openid',
            },
        });
        return authResult;
    }

    async login() {
        if (!(await this.isLoggedIn())) {
            await this.loginWithBrowser();
        }
    }

    async loginWithBrowser() {
        const { config } = this;
        const { domain, clientId } = config;
        const pkceAuth = new PKCEAuth(domain, clientId, this.browser.getRedirectURL());
        const authorizationUrl = pkceAuth.buildAuthorizeUrl(this.params);
        const responseUrl = this.browser.getResponseURL(authorizationUrl);
        const authResult = await pkceAuth.handleCallback(responseUrl);
        return authResult;
    }
}

export default Auth0;
