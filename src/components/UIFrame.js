import autobind from 'core-decorators/lib/autobind';
import React from 'react';
import cn from 'classnames';
import Image from './Image';
import Text from './Text';


export default function UIFrame({ frame, isActive = false }) {
    return (
        <div className={cn("ui-frame", {
            "ui-frame--active": isActive
        })}>
            {
                frame.type === 'image'
                    ? <Image url={frame.thumbnail} />
                    : <Text data={frame.text} />
            }
        </div>
    );
};
