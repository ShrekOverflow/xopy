/* The client side of electron which talks only to client stuff */
import { ipcRenderer } from 'electron';
import CoreRuntime from '../CoreRuntime';

// Maybe use composition instead of inheritance.
class ElectronClientRuntime extends CoreRuntime {
    constructor(config) {
        super(config);

        // In electron the pasteboard ui is opened by a global
        // shortcut, the main process is listening for it and
        // handles the window and process management

        ipcRenderer.on('open', () => {
            this.emit('open');
        });

        ipcRenderer.on('next', () => {
            this.emit('next');
        });

        this.on('release', () => {
            ipcRenderer.send('release');
        });
    }
}

export default ElectronClientRuntime;
